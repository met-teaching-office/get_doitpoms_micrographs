# get_doitpoms_micrographs

A script to download the large micrograph images from the [DoITPoMS library](https://www.doitpoms.ac.uk/miclib/index.php).

## Getting started

You will need to install the right packages, e.g.:
`pip install tqdm requests bs4 urllib pathlib`

## Running

quite simply run the script and it will stick all the micrographs into ~/Pictures/Slides. Edit the script to put them somewhere else.

It skips any existing micrographs soit can be scheduled to download new ones without eating a bunch of bandwidth.
